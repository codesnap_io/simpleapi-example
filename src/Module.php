<?php
/**
 * @package   SimpleAPI
 * @author    Uta Marian - Alexandru
 */

namespace _MODULE;

use \_MODULE\_DB as _CONN;

/**
 * Class Module
 * @package Application
 *
 * Available Methods
 * get, post, put, patch, delete, copy, head, options, link, unlink, purge, lock, unlock, profind, view
 */
class Example extends \_SIMPLEAPI\_INIT
{

    /**
     * Return a list with all the entries from the example table
     */
    static public function getListAction()
    {
        $example = new _CONN\Example();
        $exampleAll = $example->all();
        return [
            'status' => true,
            'message' => NULL,
            'list' => array_reverse($exampleAll)
        ];
    }


    /**
     * Add new random row into the database
     */
    static public function getGenerateAction()
    {
        $example = new _CONN\Example();
        $example->example_title = rand();
        $example->example_description = rand();
        $example->create();
        return [
            'status' => true,
            'message' => NULL,
        ];
    }


    /**
     * Remove a row from the database
     */
    static public function postDeleteAction()
    {
        $_DATA = \_SIMPLEAPI\_REQUEST::_POST();

        $example = new _CONN\Example();
        $id = isset($_DATA['id']) ? $_DATA['id'] : NULL;
        if ($id):
            if($example->delete($id)):
                $status = true;
            else:
                $status = false;
            endif;
        else:
            $status = false;
        endif;
        return [
            'status' => $status,
            'message' => NULL,
        ];
    }
}