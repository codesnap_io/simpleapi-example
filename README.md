#SimpleAPI - Example Module

In order to install this module you will have to:
* Copy this module directory into the /module/ directory from the SimpleAPI
* Run the following sql code :

 
CREATE TABLE `example` (
 `example_id` int(11) NOT NULL,
 `example_title` varchar(250) NOT NULL,
 `example_description` text NOT NULL
 ) ENGINE=InnoDB DEFAULT CHARSET=utf8;

 ALTER TABLE `example`
 ADD PRIMARY KEY (`example_id`);
 
 ALTER TABLE `example`
 MODIFY `example_id` int(11) NOT NULL AUTO_INCREMENT;

## Routing examples
>**/example/list** - will display all the rows from the example table

>/example/generate - generate a random row

>/example/delete - can be used by posting an **id** to the /example/delete slug

## Documentation

 [Wiki documentation](https://codesnap.io/wiki/simple-api/)

## Support

For answers you might not find in the Wiki, feel free to send me an email at *hello@codesnap.io*.