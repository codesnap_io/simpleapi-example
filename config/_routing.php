<?php
/**
 * @package   SimpleAPI
 * @author    Uta Marian - Alexandru
 */

/**
 * Example routing system
 *
 * Static route e.g. /example/list
 *
 * Available Methods
 * get, post, put, patch, delete, copy, head, options, link, unlink, purge, lock, unlock, profind, view
 */
return [
    'example/list' => [
        'module' => 'Example',
        'action' => 'List',
        'type' => 'static',
        'methods' => [
            'get'
        ]
    ],
    'example/generate' => [
        'module' => 'Example',
        'action' => 'Generate',
        'type' => 'static',
        'methods' => [
            'get'
        ]
    ],
    'example/delete' => [
        'module' => 'Example',
        'action' => 'Delete',
        'type' => 'static',
        'methods' => [
            'post'
        ]
    ],
];