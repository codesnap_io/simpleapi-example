<?php
/**
 * @package   SimpleAPI
 * @author    Uta Marian - Alexandru
 */

/**
 * Default module configuration
 */
return [
    'database' => [
        'default'
    ]
];