<?php
/**
 * @package   SimpleAPI
 * @author    Uta Marian - Alexandru
 */

/**
 * Database connection for default plugin
 */

namespace _MODULE\_DB;

class Example Extends \_SIMPLEAPI\_CRUD
{
    //:: Table name
    protected $table = 'example';

    //:: Primary key
    protected $key = 'example_id';
}
